/**
 * Constants for route names.
 */
const routes = {
  HOME: '/'
};

export default routes;
