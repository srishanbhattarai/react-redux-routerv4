const initialWelcomeState = {
  text: 'Hello World!'
};

const welcomeReducer = (state = initialWelcomeState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};

export default welcomeReducer;
