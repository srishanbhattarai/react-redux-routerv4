import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { persistStore } from 'redux-persist';
import { asyncLocalStorage } from 'redux-persist/storages';

import routes from './routes';
import configureStore from './configureStore';

const store = configureStore();

persistStore(store, { storage: asyncLocalStorage });

ReactDOM.render(
 <Provider store={ store }>
    { routes } 
  </Provider>,
 document.getElementById('root')
);
