import React from 'react';
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';

import { Home } from './components';
import routes from './constants/routes';

const browserHistory = createHistory(); 

/**
 * Central routing list.
 */
const Routes = (
  <Router history={ browserHistory }>
    <Route route={ routes.HOME } component={ Home } />
  </Router>
);

export default Routes;
