import { connect } from 'react-redux';

import Home from './Home';

const mapStateToProps = (state) => ({
  text: state.welcome.text,
});

export default connect(mapStateToProps, null)(Home);
