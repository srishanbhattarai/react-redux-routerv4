import React from 'react';
import PropTypes from 'prop-types';

const Home = (props) => {
  const { text } = props;

  return (
    <h1> { text } </h1>
  );
};

Home.propTypes = {
  text: PropTypes.string
};

Home.defaultProps = {
  text: 'Hello world!'
};

export default Home;
