import {
  createStore,
  compose,
  applyMiddleware,
} from 'redux';
import logger from 'redux-logger';
import { autoRehydrate } from 'redux-persist';

import rootReducer from './reducers';

/**
 * Middleware stack for Redux.
 */
const middleware = [
  logger,
];

/**
 * A function, when called, initializes the Redux store.
 * 
 * @returns {Object} store
 */
const configureStore = () => {
  return createStore(
    rootReducer,
    compose(
      applyMiddleware(...middleware),
      autoRehydrate()
    )
  );
};

export default configureStore;
